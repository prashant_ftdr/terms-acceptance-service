package authmiddleware

import (
	"fmt"
	"go.ftdr.com/go-utils/common/errors"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"

	"go.ftdr.com/go-utils/common/logging"
)

const (
	noTokenErr         = "bearer token required"
	tokenParseErr      = "could not parse bearer token"
	scopeNotAuthorized = "the scope of the bearer token is not authorized"
)

//AuthMiddleWare is the struct that contains the auth scope
type AuthMiddleWare struct {
	AuthScope map[string]string
	Exempt    map[string]struct{}
}

//SetAuthScopes sets auth scopes
func (amw *AuthMiddleWare) SetAuthScopes(scopes map[string]string) {
	amw.AuthScope = scopes
}

//Middleware is the handler that checks validation.
func (amw *AuthMiddleWare) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		routeName := mux.CurrentRoute(r).GetName()
		authError := isAuthorized(r.Header.Get("Authorization"), amw.AuthScope[routeName])
		_, exempt := amw.Exempt[routeName]
		if !exempt {
			err := isAuthorized(r.Header.Get("Authorization"), amw.AuthScope[routeName])
			if err != nil {
				fields := map[string]interface{}{
					"Error":  authError.Error(),
					"Client": r.RemoteAddr,
					"Uri":    r.RequestURI,
				}
				logging.Log.WithFields(fields).Info(errors.UnauthorizedTokenError)
				http.Error(w, authError.Error(), http.StatusUnauthorized)
				return
			}
		}
		next.ServeHTTP(w, r)
	})
}

func isAuthorized(token string, authScope string) (err error) {
	if token == "" {
		return fmt.Errorf(noTokenErr)
	}

	chunks := strings.SplitN(token, " ", 2)
	if len(chunks) == 2 {
		token = chunks[1]
	}

	jwtToken, err := jwt.Parse(token, nil)

	if jwtToken == nil {
		return fmt.Errorf(tokenParseErr)
	}

	claimsScope := jwtToken.Claims.(jwt.MapClaims)["scope"]
	if claimsScope == nil {
		return fmt.Errorf(tokenParseErr)
	}

	scopes := claimsScope.([]interface{})
	for _, scope := range scopes {
		if scope == authScope {
			return nil
		}
	}

	return fmt.Errorf(scopeNotAuthorized)
}
