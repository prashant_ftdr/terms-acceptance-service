module terms-acceptance-service

go 1.13

//replace google.golang.org/protobuf v1.25.0 => ../protos

require (
	bou.ke/monkey v1.0.2
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/golang/protobuf v1.4.2
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.3.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/mitchellh/mapstructure v1.3.3
	github.com/opentracing/opentracing-go v1.2.0
	github.com/sirupsen/logrus v1.6.0
	github.com/stretchr/testify v1.6.1
	github.com/uber/jaeger-client-go v2.25.0+incompatible
	go.ftdr.com/go-utils/common v0.1.146
	go.ftdr.com/go-utils/instrumentation/v2 v2.0.3
	golang.frontdoorhome.com/software/protos v1.0.589
	google.golang.org/protobuf v1.25.0
)
