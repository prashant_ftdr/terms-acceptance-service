package literals

const (
	LLEntryPoint       = "EntryPoint"
	LLHTTPResponseCode = "HTTPResponseCode"
	LLErrorCode        = "errorCode"
	LLErrorMsg         = "errorMsg"
)
