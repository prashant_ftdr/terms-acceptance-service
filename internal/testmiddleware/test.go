package testmiddleware

import (
	"io/ioutil"
	"net/http"

	"terms-acceptance-service/internal/apperrors"
	"terms-acceptance-service/internal/literals"
	"terms-acceptance-service/internal/util"

	"golang.frontdoorhome.com/software/protos/go/common"
	"golang.frontdoorhome.com/software/protos/go/termsacceptance"

	"go.ftdr.com/go-utils/common/logging"

	"github.com/gorilla/mux"
	"github.com/mitchellh/mapstructure"
	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/proto"
)

//TestMiddleware is the struct that contains the request and response proto object maps
type TestMiddleware struct {
	responseDir      string
	exceptionMap     map[string]bool
	requestProtoMap  map[string]proto.Message
	responseProtoMap map[string]proto.Message
}

// Init initializes the request and response maps with empty instances
func Init(responseDir string) *TestMiddleware {
	testMiddleware := &TestMiddleware{}
	testMiddleware.responseDir = responseDir
	testMiddleware.exceptionMap = map[string]bool{
		literals.HealthcheckAPIName: true,
	}
	testMiddleware.requestProtoMap = map[string]proto.Message{
		literals.FetchTAStatusAPIName: &termsacceptance.TermsAcceptanceRequest{},
	}
	testMiddleware.responseProtoMap = map[string]proto.Message{
		literals.FetchTAStatusAPIName: &termsacceptance.TermsAcceptanceResponse{},
	}

	return testMiddleware
}

//Middleware checks if tenantID is TEST in request then it returns response
func (tm *TestMiddleware) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		apiName := mux.CurrentRoute(r).GetName()

		if _, ok := tm.exceptionMap[apiName]; !ok {
			protoReq, ok := tm.requestProtoMap[apiName]
			if !ok {
				logging.Log.WithField("API Name", apiName).Error(apperrors.APINotSupportedError.Error())
				http.Error(w, apperrors.APINotSupportedError.Error(), http.StatusInternalServerError)
				return
			}

			err := util.UnmarshalRequest(r, protoReq, logging.GetTracedLogEntry(r.Context()))
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}

			protoMap := make(map[string]interface{})
			err = mapstructure.Decode(protoReq, &protoMap)
			if err != nil {
				logging.Log.WithField("API Name", apiName).Error(apperrors.ProtoToMapConversionError, ". ", err)
				http.Error(w, apperrors.ProtoToMapConversionError.Error(), http.StatusInternalServerError)
				return
			}

			if tenant, tenantExists := protoMap[literals.ProtoParamTenantID]; tenantExists {

				if tenant == common.Tenant_TEST {

					protoResp, respExists := tm.responseProtoMap[apiName]
					if !respExists {
						logging.Log.WithField("API Name", apiName).Error(apperrors.APINotSupportedError.Error())
						http.Error(w, apperrors.APINotSupportedError.Error(), http.StatusInternalServerError)
						return
					}

					jsonResp, err := ioutil.ReadFile(tm.responseDir + apiName + ".json")
					if err != nil {
						logging.Log.WithField("API Name", apiName).Error(err)
						http.Error(w, apperrors.FileReadError.Error(), http.StatusInternalServerError)
						return
					}

					err = protojson.Unmarshal(jsonResp, protoResp)
					if err != nil {
						logging.Log.WithField("API Name", apiName).Error(err)
						http.Error(w, apperrors.JSONUnmarshalError.Error(), http.StatusInternalServerError)
						return
					}
					util.WriteProtoToResponse(r.Context(), logging.GetTracedLogEntry(r.Context()), w, protoResp, 200)
					return
				}
			}
		}

		next.ServeHTTP(w, r)
	})
}
