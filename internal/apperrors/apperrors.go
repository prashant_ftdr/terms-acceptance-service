package apperrors

import (
	"go.ftdr.com/go-utils/common/errors"
	"net/http"
	"terms-acceptance-service/internal/literals"
)

const (
	envPrefix = "TAS"
)

// Category - BLE
var (
	// RequestBodyReadError Failed to read data from the request body
	RequestBodyReadError = errors.ServerError{
		Message:          literals.RequestBodyReadError,
		Code:             envPrefix + "_BLE_0002",
		HTTPResponseCode: http.StatusInternalServerError,
	}
	// ProtoUnmarshalError Failed to unmarshal byte array to proto struct
	ProtoUnmarshalError = errors.ServerError{
		Message:          literals.ProtoUnmarshalError,
		Code:             envPrefix + "_BLE_0003",
		HTTPResponseCode: http.StatusInternalServerError,
	}
	// ProtoMarshalError Failed to marshal proto struct to byte array
	ProtoMarshalError = errors.ServerError{
		Message:          literals.ProtoMarshalError,
		Code:             envPrefix + "_BLE_0004",
		HTTPResponseCode: http.StatusInternalServerError,
	}
	// EncoderError Could not encode the response
	EncoderError = errors.ServerError{
		Message:          literals.EncoderError,
		Code:             envPrefix + "_BLE_0005",
		HTTPResponseCode: http.StatusInternalServerError,
	}
	// UUIDParseFailedError Failed to parse uuid
	UUIDParseFailedError = errors.ServerError{
		Message:          literals.UUIDParseFailedError,
		Code:             envPrefix + "_BLE_0006",
		HTTPResponseCode: http.StatusBadRequest,
	}
	// InvalidRequestDataError Invalid request data
	InvalidRequestDataError = errors.ServerError{
		Message:          literals.InvalidRequestDataError,
		Code:             envPrefix + "_BLE_0007",
		HTTPResponseCode: http.StatusBadRequest,
	}
	// Base64DecodeError Failed to decode base64 url query param
	Base64DecodeError = errors.ServerError{
		Message:          literals.Base64DecodeError,
		Code:             envPrefix + "_BLE_0008",
		HTTPResponseCode: http.StatusBadRequest,
	}
	// ProtoToMapConversionError Unable to convert proto message to map
	ProtoToMapConversionError = errors.ServerError{
		Message:          literals.ProtoToMapConversionError,
		Code:             envPrefix + "_BLE_0009",
		HTTPResponseCode: http.StatusInternalServerError,
	}
	// MissingUserID Logged-in user ID is either missing or passed as empty value in request data
	MissingUserID = errors.ServerError{
		Message:          literals.MissingUserID,
		Code:             envPrefix + "_BLE_0010",
		HTTPResponseCode: http.StatusBadRequest,
	}
	// MissingUserRole User Role is either missing or passed as empty value in request data
	MissingUserRole = errors.ServerError{
		Message:          literals.MissingUserRole,
		Code:             envPrefix + "_BLE_0011",
		HTTPResponseCode: http.StatusBadRequest,
	}
	// MissingTenant Tenant is either missing or passed as empty value in request data
	MissingTenant = errors.ServerError{
		Message:          literals.MissingTenant,
		Code:             envPrefix + "_BLE_0012",
		HTTPResponseCode: http.StatusBadRequest,
	}
	// InvalidUserID User ID must be a valid UUID
	InvalidUserID = errors.ServerError{
		Message:          literals.InvalidUserID,
		Code:             envPrefix + "_BLE_0013",
		HTTPResponseCode: http.StatusBadRequest,
	}
	// InvalidUserRole Operation not allowed for user role
	InvalidUserRole = errors.ServerError{
		Message:          literals.InvalidUserRole,
		Code:             envPrefix + "_BLE_0014",
		HTTPResponseCode: http.StatusBadRequest,
	}
	// InvalidTenant Tenant is invalid
	InvalidTenant = errors.ServerError{
		Message:          literals.InvalidTenant,
		Code:             envPrefix + "_BLE_0015",
		HTTPResponseCode: http.StatusBadRequest,
	}
	// APINotSupportedError API is not supported
	APINotSupportedError = errors.ServerError{
		Message:          literals.APINotSupportedError,
		Code:             envPrefix + "_BLE_0016",
		HTTPResponseCode: http.StatusInternalServerError,
	}
	// FileReadError Unable to read file
	FileReadError = errors.ServerError{
		Message:          literals.FileReadError,
		Code:             envPrefix + "_BLE_0017",
		HTTPResponseCode: http.StatusInternalServerError,
	}
	// JSONUnmarshalError Failed to unmarshal struct to JSON string
	JSONUnmarshalError = errors.ServerError{
		Message:          literals.JSONUnmarshalError,
		Code:             envPrefix + "_BLE_0018",
		HTTPResponseCode: http.StatusInternalServerError,
	}
)
