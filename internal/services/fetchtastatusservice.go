package services

import (
	"context"

	"sync"

	"terms-acceptance-service/internal/util"

	"golang.frontdoorhome.com/software/protos/go/common"
	"golang.frontdoorhome.com/software/protos/go/termsacceptance"
)

// FetchTAStatusService ...
type FetchTAStatusService interface {
	ValidateRequest(ctx context.Context, req *termsacceptance.TermsAcceptanceRequest) (
		validationErrors []*common.Error)
	ProcessRequest(ctx context.Context, req *termsacceptance.TermsAcceptanceRequest) (
		*termsacceptance.TermsAcceptanceResponse, error)
}

var fetchTAStatusSvcStruct FetchTAStatusService
var fetchTAStatusServiceOnce sync.Once

// fetchTAStatusService ...
type fetchTAStatusService struct {
	config *util.RouterConfig
}

// InitFetchTAStatusService ...
func InitFetchTAStatusService(config *util.RouterConfig) FetchTAStatusService {
	fetchTAStatusServiceOnce.Do(func() {
		fetchTAStatusSvcStruct = &fetchTAStatusService{config: config}
	})
	return fetchTAStatusSvcStruct
}

// GetFetchTAStatusService ...
func GetFetchTAStatusService() FetchTAStatusService {
	if fetchTAStatusSvcStruct == nil {
		panic("FetchTAStatusService not initialized")
	}
	return fetchTAStatusSvcStruct
}

// ValidateRequest ...
func (service *fetchTAStatusService) ValidateRequest(ctx context.Context, req *termsacceptance.TermsAcceptanceRequest) (
	validationErrors []*common.Error) {
	// TODO: Add validation logic here
	return nil
}

// ProcessRequest ...
func (service *fetchTAStatusService) ProcessRequest(ctx context.Context, req *termsacceptance.TermsAcceptanceRequest) (
	*termsacceptance.TermsAcceptanceResponse, error) {
	// TODO: Add business logic here
	return &termsacceptance.TermsAcceptanceResponse{}, nil
}
