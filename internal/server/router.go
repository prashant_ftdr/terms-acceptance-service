package server

import (
	"github.com/gorilla/mux"
	"go.ftdr.com/go-utils/common/middleware"
	instrumentation "go.ftdr.com/go-utils/instrumentation/v2"
	"net/http"

	"terms-acceptance-service/internal/config"
	"terms-acceptance-service/internal/literals"
	"terms-acceptance-service/internal/services"
	"terms-acceptance-service/internal/testmiddleware"
	"terms-acceptance-service/internal/util"
)

const (
	protobufContentType = "application/x-protobuf"
)

var (
	routeAcceptedContents = map[string]string{
		literals.HealthcheckAPIName:   protobufContentType,
		literals.FetchTAStatusAPIName: protobufContentType,
	}

	authExemptEndpoints = map[string]struct{}{
		literals.HealthcheckAPIName: struct{}{},
	}
)

// Router ...
type Router struct {
	*mux.Router
}

// NewRouter ...
func NewRouter() *Router {
	return &Router{mux.NewRouter()}
}

// InitializeRouter ...
func (r *Router) InitializeRouter(routerConfig *util.RouterConfig) {
	r.initializeMiddleware(routerConfig.WebServerConfig)
	r.initializeRoutes(routerConfig)
}

// initializeRoutes ...
func (r *Router) initializeRoutes(routerConfig *util.RouterConfig) {
	s := (*r).PathPrefix(routerConfig.WebServerConfig.RoutePrefix).Subrouter()

	s.HandleFunc(literals.HealthcheckUri,
		HealthCheckHandler(routerConfig)).
		Methods(http.MethodGet).
		Name(literals.HealthcheckAPIName)

	s.HandleFunc(literals.FetchTAStatusURI,
		FetchTAStatusHandler(services.GetFetchTAStatusService(), routerConfig)).
		Methods(http.MethodOptions, http.MethodGet).
		Name(literals.FetchTAStatusAPIName)

}

// initializeMiddleware ..
func (r *Router) initializeMiddleware(serverConfig *config.WebServerConfig) {
	if serverConfig.EnableJaegerLogs {
		r.Use(middleware.TracerMiddleware)
	}
	r.Use(middleware.TraceMiddleware)
	r.Use(instrumentation.Middleware)
	r.Use(middleware.LoggingMiddleware)

	vmw := middleware.ValidatorMiddleWare{}
	vmw.SetAcceptedContent(routeAcceptedContents)
	r.Use(vmw.Middleware)

	r.Use(middleware.ReadReqMiddleware)
	r.Use(testmiddleware.Init(serverConfig.ResponseDirPath).Middleware)
	r.Use(middleware.Recovery)
}
