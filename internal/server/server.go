package server

import (
	"fmt"
	opentracing "github.com/opentracing/opentracing-go"
	jaeger "github.com/uber/jaeger-client-go"
	jaegerconfig "github.com/uber/jaeger-client-go/config"
	instrumentation "go.ftdr.com/go-utils/instrumentation/v2"
	"io"
	"net/http"
	"net/url"
	"terms-acceptance-service/internal/config"

	"terms-acceptance-service/internal/services"
	"terms-acceptance-service/internal/util"

	"go.ftdr.com/go-utils/common/logging"
	commonServices "go.ftdr.com/go-utils/common/services"
)

const (
	dependencyURL = "https://gitlab.com/ftdr/software/terms-acceptance-service/"
)

//Server ...
type Server struct {
	Configuration *config.WebServerConfig
	Router        *Router
}

//NewServer ...
func NewServer(config *config.WebServerConfig) *Server {
	server := &Server{
		Configuration: config,
		Router:        NewRouter(),
	}

	return server
}

//RunServer ...
func RunServer() (err error) {
	webServerConfig, logCfg, ciCfg, err := config.FromEnv()
	if err != nil {
		return err
	}

	err = logging.Initialize(logCfg)
	if err != nil {
		return err
	}

	baseURL, err := url.Parse(dependencyURL)
	if err != nil {
		logging.Log.WithError(err).Error()
		return err
	}

	routerConfigs := util.RouterConfig{
		WebServerConfig: webServerConfig,
		CIConfig:        ciCfg,
		DependencyURL:   *baseURL,
	}

	// initialize jaeger depending upon config value
	if webServerConfig.EnableJaegerLogs {
		tracer, closer := initJaeger("terms-acceptance-service")
		defer closer.Close()
		opentracing.SetGlobalTracer(tracer)
	}

	// Initializing services
	initServices(&routerConfigs)

	// initialize instrumentation
	instrumentation.Init("terms-acceptance-service", webServerConfig.Instrumentation)
	defer instrumentation.Close(webServerConfig.Instrumentation)

	server := NewServer(webServerConfig)
	server.Router.InitializeRouter(&routerConfigs)

	cors := commonServices.CORSSetup()

	logging.Log.Infof("Starting HTTP server on port %s", webServerConfig.Port)
	err = http.ListenAndServe(":"+webServerConfig.Port, cors.Handler(server.Router))
	if err != nil {
		return err
	}

	return nil
}

// Initializes services by calling respective Init method
func initServices(routerConf *util.RouterConfig) {

	services.InitFetchTAStatusService(routerConf)

}

// initJaeger returns an instance of Jaeger Tracer that samples 100% of traces and logs all spans to stdout.
func initJaeger(service string) (opentracing.Tracer, io.Closer) {
	cfg := &jaegerconfig.Configuration{
		Sampler: &jaegerconfig.SamplerConfig{
			Type:  "const",
			Param: 1,
		},
		Reporter: &jaegerconfig.ReporterConfig{
			LogSpans: true,
		},
	}
	cfg, err := cfg.FromEnv()
	if err != nil {
		panic(fmt.Sprintf("ERROR: cannot init Jaeger: %v\n", err))
	}
	tracer, closer, err := cfg.New(service, jaegerconfig.Logger(jaeger.StdLogger))
	if err != nil {
		panic(fmt.Sprintf("ERROR: cannot init Jaeger: %v\n", err))
	}
	return tracer, closer
}
