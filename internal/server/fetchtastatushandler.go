package server

import (
	"net/http"

	"go.ftdr.com/go-utils/common/errors"
	"go.ftdr.com/go-utils/common/logging"

	"terms-acceptance-service/internal/services"
	"terms-acceptance-service/internal/util"

	"golang.frontdoorhome.com/software/protos/go/common"
	"golang.frontdoorhome.com/software/protos/go/termsacceptance"
)

// FetchTAStatusHandler  Fetch tas status for a given tas version id @Scope read
func FetchTAStatusHandler(service services.FetchTAStatusService, config *util.RouterConfig) http.HandlerFunc {
	return func(w http.ResponseWriter, request *http.Request) {

		var response *termsacceptance.TermsAcceptanceResponse

		ctx := request.Context()

		log := logging.GetTracedLogEntry(ctx)

		reqData := &termsacceptance.TermsAcceptanceRequest{}
		err := util.UnmarshalAndValidateRequest(request, reqData, log, config)
		if err != nil {
			response = getFetchTAStatusErrorResponse(err)
			util.WriteProtoToResponse(ctx, log, w, response, http.StatusBadRequest)
			return
		}

		// Log the request context
		// TODO : Add the fields as required

		errs := service.ValidateRequest(ctx, reqData)

		if errs != nil {
			response = getFetchTAStatusValidationErrorResponse(errs)
			util.WriteProtoToResponse(ctx, log, w, response, http.StatusBadRequest)
			return
		}

		responseMsg, err := service.ProcessRequest(ctx, reqData)

		httpStatusCode := http.StatusOK

		if err != nil {
			response = getFetchTAStatusErrorResponse(err)
			httpStatusCode = err.(*errors.ServerError).HTTPResponseCode
		} else {
			response = responseMsg
			response.Status = common.Status_SUCCESS
		}

		util.WriteProtoToResponse(ctx, log, w, response, httpStatusCode)

	}
}

func getFetchTAStatusErrorResponse(err error) *termsacceptance.TermsAcceptanceResponse {
	errProto := util.ServerToProtoError(err)
	response := &termsacceptance.TermsAcceptanceResponse{
		Status: common.Status_FAIL,
		Errors: []*common.Error{errProto},
	}

	return response
}

func getFetchTAStatusValidationErrorResponse(errs []*common.Error) *termsacceptance.TermsAcceptanceResponse {
	response := &termsacceptance.TermsAcceptanceResponse{
		Status: common.Status_FAIL,
		Errors: errs,
	}

	return response
}
