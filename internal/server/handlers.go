package server

import (
	"net/http"

	"go.ftdr.com/go-utils/common/handlers"

	"terms-acceptance-service/internal/util"
)

// HealthCheckHandler ..
func HealthCheckHandler(routerConf *util.RouterConfig) http.HandlerFunc {
	return handlers.HealthCheckHandler(routerConf.CIConfig.EnvironmentName, routerConf.CIConfig.CommitShortSHA,
		routerConf.DependencyURL)
}
