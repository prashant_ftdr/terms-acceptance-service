package server

import (
	"net/http"
	"terms-acceptance-service/internal/util"

	"bou.ke/monkey"
	"google.golang.org/protobuf/reflect/protoreflect"
)

func patchValidateRequest() {
	monkey.Patch(util.ValidateRequest, func(*http.Request, protoreflect.ProtoMessage, *util.RouterConfig) error {
		return nil
	})
}

func unpatchValidateRequest() {
	monkey.Unpatch(util.ValidateRequest)
}
