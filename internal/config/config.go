package config

import (
	"fmt"

	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
	"go.ftdr.com/go-utils/common/logging"
	"go.ftdr.com/go-utils/instrumentation/v2"
)

const (
	envPrefix = "TAS"
	ciPrefix  = "CI"
)

//WebServerConfig ...
type WebServerConfig struct {
	Port             string `required:"true" split_words:"true" default:"50051"`
	RoutePrefix      string `required:"false" split_words:"true" default:""`
	AllowedTenants   string `required:"false" split_words:"true" default:"AHS,CANDU"`
	ResponseDirPath  string `required:"false" split_words:"true" default:"../../internal/testmiddleware/responses/"`
	EnableJaegerLogs bool   `required:"false" split_words:"true" default:"false"`
	JaegerEndpoint   string `required:"false" split_words:"true" default:""`
	Instrumentation  *instrumentation.Config
}

// CIConfig ...
type CIConfig struct {
	CommitShortSHA  string `required:"true" split_words:"true" default:"dev-local"`
	EnvironmentName string `required:"true" split_words:"true" default:"dev-local"`
}

//FromEnv ...
func FromEnv() (cfg *WebServerConfig, lcfg *logging.Config, cicfg *CIConfig, err error) {
	fromFileToEnv()

	cfg = &WebServerConfig{}

	err = envconfig.Process(envPrefix, cfg)

	if err != nil {

		return nil, nil, nil, err
	}

	cicfg = &CIConfig{}
	err = envconfig.Process(ciPrefix, cicfg)
	if err != nil {
		return nil, nil, nil, err
	}

	lcfg = &logging.Config{}
	err = envconfig.Process("", lcfg)
	if err != nil {

		return nil, nil, nil, err
	}

	return cfg, lcfg, cicfg, nil
}

func fromFileToEnv() {

	cfgFilename := "../../etc/config/config.localhost.env"

	err := godotenv.Load(cfgFilename)
	if err != nil {
		fmt.Println("No config files found to load to env. Defaulting to environment.")
	}

}
