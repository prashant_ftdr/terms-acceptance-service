package util

import (
	"bytes"
	"context"
	"net/http"
	"net/http/httptest"
	"sync"
	"terms-acceptance-service/internal/config"

	"github.com/google/uuid"
	"go.ftdr.com/go-utils/common/constants"
	instrumentation "go.ftdr.com/go-utils/instrumentation/v2"
)

var createConf sync.Once
var conf *RouterConfig

// GetConf ..
func GetConf() *RouterConfig {
	createConf.Do(func() {
		webServerConf, _, _, _ := config.FromEnv()
		conf = &RouterConfig{WebServerConfig: webServerConf}
	})
	return conf
}

// GetInstrumentationContext ..
func GetInstrumentationContext() context.Context {
	ctx := context.Background()
	ctx = context.WithValue(ctx, "entrypoint", "test")
	ctx = context.WithValue(ctx, constants.TraceIDContextKey, "test")
	instrumentation.Init("terms-acceptance-service", &instrumentation.Config{EnableJaeger: true, ChannelLimit: 1000})
	instrumentation.Start(ctx)
	return ctx
}

// InstrumentationSuccess ..
func InstrumentationSuccess(ctx context.Context) {
	instrumentation.Success(ctx, nil)
}

// PrepHandlerTest ...
func PrepHandlerTest(validTraceID bool, reqData []byte) (*http.Request, *httptest.ResponseRecorder) {
	testRequest := new(http.Request)
	ctx := GetInstrumentationContext()
	if validTraceID {
		ctx = context.WithValue(ctx, constants.TraceIDContextKey, uuid.New().String())
	} else {
		ctx = context.WithValue(ctx, constants.TraceIDContextKey,
			"123456789012345678901234567890123456789012345678901234567890")
	}
	ctx = context.WithValue(ctx, constants.ProtoKey, reqData)
	testRequest = testRequest.WithContext(ctx)
	testWriter := new(httptest.ResponseRecorder)
	testResponseBodyBuffer := new(bytes.Buffer)
	testWriter.Body = testResponseBodyBuffer
	return testRequest, testWriter
}
