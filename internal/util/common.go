package util

import (
	"bytes"
	"context"
	"encoding/base64"
	"net/http"
	"net/url"
	"strings"

	"terms-acceptance-service/internal/apperrors"
	"terms-acceptance-service/internal/config"
	"terms-acceptance-service/internal/literals"

	"github.com/google/uuid"
	"github.com/mitchellh/mapstructure"
	"github.com/sirupsen/logrus"
	"go.ftdr.com/go-utils/common/constants"
	"go.ftdr.com/go-utils/common/errors"

	"go.ftdr.com/go-utils/common/logging"
	"golang.frontdoorhome.com/software/protos/go/common"
	"google.golang.org/protobuf/proto"
)

const (
	secondsInOneYear    = "31536000"
	protoMsgURLParamKey = "proto_body"
	base64PaddingChar   = "="
)

// RouterConfig Struct which stores all app dependencies required by different components
type RouterConfig struct {
	WebServerConfig *config.WebServerConfig
	CIConfig        *config.CIConfig
	DependencyURL   url.URL
}

// IsValidUUID Checks if the passed string is a valid UUID or not
func IsValidUUID(uid string) bool {
	if len(uid) > 0 {
		if literals.EmptyUUID == uid {
			return false
		}
		_, err := uuid.Parse(uid)
		return err == nil
	}
	return false
}

// base64Decode attempts to decode the bytes using RawStdEncoding & RawURLEncoding techniques.
// If none of them work, it throws an error.
func base64Decode(data []byte, log *logrus.Entry) ([]byte, error) {

	var b64StdDecError error
	var b64URLDecError error

	// Strip all padding characters ('=') from byte data, if any.
	// NOTE: In Base64 encoding, the length of output encoded string must be a multiple of 3.
	// If it's not, the output will be padded with additional pad characters (`=`).
	// Hence, any base64 encoded data can have atmost 2 padding characters.
	data = bytes.TrimSuffix(data, []byte(base64PaddingChar))
	data = bytes.TrimSuffix(data, []byte(base64PaddingChar))

	// base64 decode using RawStdEncoding
	b64DecodedData := make([]byte, base64.RawStdEncoding.DecodedLen(len(data)))
	_, b64StdDecError = base64.RawStdEncoding.Decode(b64DecodedData, data)
	if b64StdDecError == nil {
		return b64DecodedData, nil
	}

	// base64 decode using RawURLEncoding
	b64DecodedData = make([]byte, base64.RawURLEncoding.DecodedLen(len(data)))
	_, b64URLDecError = base64.RawURLEncoding.Decode(b64DecodedData, data)
	if b64URLDecError == nil {
		return b64DecodedData, nil
	}

	log.Error("Base64 decode failed with both standard and url encoding techniques")
	log.Errorf("With RawStdEncoding, got error: %v", b64StdDecError.Error())
	log.Errorf("With RawURLEncoding, got error: %v", b64URLDecError.Error())
	return nil, &apperrors.Base64DecodeError
}

// UnmarshalRequest Unmarshals the proto request
func UnmarshalRequest(r *http.Request, target proto.Message, log *logrus.Entry) (err error) {
	var data []byte

	if r.Method == http.MethodGet || r.Method == http.MethodDelete {
		urlData, ok := r.URL.Query()[protoMsgURLParamKey]
		if !ok || len(urlData[0]) < 1 {
			return nil
		}

		encodedProtoData := []byte(urlData[0])
		var err error
		data, err = base64Decode(encodedProtoData, log)
		if err != nil {
			return err
		}
	} else {
		ctx := r.Context()
		protoData := ctx.Value(constants.ProtoKey)
		if protoData == nil {
			log.Error("Error while reading from request body: ", err)
			return &apperrors.RequestBodyReadError
		}

		data = protoData.([]byte)
	}

	err = proto.Unmarshal(data, target)
	if err != nil {
		log.Error("Error while unmarshaling proto: ", err)
		return &apperrors.ProtoUnmarshalError
	}

	return nil
}

// ValidateRequest Validate the request for tenant and logged-in user info
func ValidateRequest(r *http.Request, target proto.Message, routerConf *RouterConfig) error {
	protoMap := make(map[string]interface{})
	err := mapstructure.Decode(target, &protoMap)
	if err != nil {
		logging.Log.Error(apperrors.ProtoToMapConversionError, ". ", err)
		return &apperrors.ProtoToMapConversionError
	}

	// Check if unmarshalled message has logged-in user info
	// Expected field names - GET: UserID, POST: CreatedBy, PUT: UpdatedBy
	userIDField := literals.ProtoParamUserID
	if r.Method == http.MethodPost {
		userIDField = literals.ProtoParamCreatedBy
	} else if r.Method == http.MethodPut {
		userIDField = literals.ProtoParamUpdatedBy
	}

	if userID, ok := protoMap[userIDField]; ok {
		if userID == "" {
			return &apperrors.MissingUserID
		} else if !IsValidUUID(userID.(string)) {
			return &apperrors.InvalidUserID
		}
	} else {
		return &apperrors.MissingUserID
	}

	if userRole, ok := protoMap[literals.ProtoParamUserRole]; ok {
		_, roleExists := common.UserRole_name[int32(userRole.(common.UserRole))]
		if !roleExists {
			return &apperrors.InvalidUserRole
		} else if userRole == common.UserRole_USER_ROLE_UNDEFINED {
			return &apperrors.MissingUserRole
		}
	} else {
		return &apperrors.MissingUserRole
	}

	// Check if unmarshalled message has tenant info
	allowedTenants := routerConf.WebServerConfig.AllowedTenants
	tenantArr := strings.Split(allowedTenants, ",")
	if tenant, ok := protoMap[literals.ProtoParamTenantID]; ok {
		tenantName, tenantExists := common.Tenant_name[int32(tenant.(common.Tenant))]
		if !tenantExists {
			return &apperrors.InvalidTenant
		} else if tenant == common.Tenant_TENANT_UNDEFINED {
			return &apperrors.MissingTenant
		}

		isValidTenant := false
		for _, allowedTenant := range tenantArr {
			if tenantName == allowedTenant {
				isValidTenant = true
				break
			}
		}
		if !isValidTenant {
			return &apperrors.InvalidTenant
		}
	} else {
		return &apperrors.MissingTenant
	}

	return nil
}

// UnmarshalAndValidateRequest Unmarshals and validates the request
func UnmarshalAndValidateRequest(r *http.Request, target proto.Message, log *logrus.Entry,
	routerConf *RouterConfig) (err error) {

	err = UnmarshalRequest(r, target, log)
	if err != nil {
		return err
	}

	err = ValidateRequest(r, target, routerConf)
	if err != nil {
		log.Error("Error while validating request: ", err)
		return err
	}

	return nil
}

// SetResponseHeader ..
func SetResponseHeader(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/x-protobuf")
	w.Header().Set("Content-Security-Policy", "default-src 'self';")
	w.Header().Set("Strict-Transport-Security", "max-age="+secondsInOneYear)
}

// LogAndSendHTTPError ..
func LogAndSendHTTPError(log *logrus.Entry, w http.ResponseWriter, error error, serverError *errors.ServerError,
	statusCode int) {
	if log != nil {
		log.WithError(error).Error(serverError)
	}
	http.Error(w, serverError.Error(), statusCode)
}

// WriteProtoToResponse ...
func WriteProtoToResponse(ctx context.Context, log *logrus.Entry, writer http.ResponseWriter, protoMsg proto.Message, statusCode int) {
	var errProto error
	if protoMsg != nil {
		response, err := proto.Marshal(protoMsg)
		if err != nil {
			statusCode = http.StatusInternalServerError
			errProto = &apperrors.ProtoMarshalError

			LogAndSendHTTPError(log, writer, err, &apperrors.ProtoMarshalError, http.StatusInternalServerError)
		}

		SetResponseHeader(writer)
		writer.WriteHeader(statusCode)
		_, err = writer.Write(response)
		if err != nil {
			statusCode = http.StatusInternalServerError
			errProto = &apperrors.EncoderError

			LogAndSendHTTPError(log, writer, err, &apperrors.EncoderError, http.StatusInternalServerError)
		}
	}

	if IsSuccess(statusCode) {
		// Log the response code for the API
		log.WithFields(logrus.Fields{
			literals.LLEntryPoint:       ctx.Value(literals.EntrypointKey).(string),
			literals.LLHTTPResponseCode: statusCode,
		}).Info(literals.APISucceeded)
	} else {
		if errProto != nil {
			log.WithFields(logrus.Fields{
				literals.LLEntryPoint:       ctx.Value(literals.EntrypointKey).(string),
				literals.LLHTTPResponseCode: statusCode,
				literals.LLErrorCode:        errProto.(*errors.ServerError).Code,
				literals.LLErrorMsg:         errProto.(*errors.ServerError).Message,
			}).Info(literals.APIFailed)
		} else {
			log.WithFields(logrus.Fields{
				literals.LLEntryPoint:       ctx.Value(literals.EntrypointKey).(string),
				literals.LLHTTPResponseCode: statusCode,
			}).Info(literals.APIFailed)
		}
	}

}

// IsSuccess reports whether statusCode is in the Successful (2xx) status code class
func IsSuccess(statusCode int) bool {
	return statusCode == 200 || statusCode == 204
}

// ServerToProtoError ...
func ServerToProtoError(err error) *common.Error {
	errorProto := &common.Error{
		Code:    err.(*errors.ServerError).Code,
		Message: err.Error(),
	}
	return errorProto
}
